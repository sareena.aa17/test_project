
class FoodModel {
  int? id;
  String foodname;
  String price;
  String image;

  FoodModel({
    this.id,
    required this.foodname,
    required this.price,
    required this.image,

  });

  factory FoodModel.fromMap(Map<String, dynamic> json) =>
      new FoodModel(
        id: json['id'],
        foodname: json['foodname'],
        price: json['price'],
        image: json['image'],
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'foodname': foodname,
      'price': price,
      'image': image,
    };
  }
}