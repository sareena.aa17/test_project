
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sqlite_project/database/database_helper.dart';
import 'package:sqlite_project/model/profile_model.dart';

class ShowFood extends StatelessWidget {
  final id;
  ShowFood({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Food'),
      ),
      body: Center(
        child: FutureBuilder<List<FoodModel>>(
            future: DatabaseHelper.instance.getFood(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<FoodModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Groceries in List.'))
                  : Padding(
                    padding: const EdgeInsets.all(40.40),
                    child: ListView(
                children: snapshot.data!.map((food) {
                    return Center(
                      child: Column(
                        children: [
                          CircleAvatar(
                            backgroundImage: FileImage(File(food.image)),
                            radius: 100,
                          ),
                          Padding(padding: const EdgeInsets.all(30.30),
                            child : Text(food.foodname,style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                          ),
                          Padding(padding: const EdgeInsets.all(8.0),
                            child: Text('Price: ${food.price} ฿'),
                          ),
                        ],
                      ),
                    );
                }).toList(),
              ),
                  );
            }),
      ),
    );
  }
}
