import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sqlite_project/database/database_helper.dart';
import 'package:sqlite_project/model/profile_model.dart';
import 'dart:developer' as developer;

class AddFood extends StatefulWidget {
  AddFood();

  @override
  _AddFoodState createState() => _AddFoodState();
}

class _AddFoodState extends State<AddFood> {
  var foodname = TextEditingController();
  var price = TextEditingController();
  var _image;
  var imagePicker;


  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Food'),
      ),
      body: Container(
        color: Colors.white,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.15),
                      child: GestureDetector(
                        onTap: () async {
                         final image = await imagePicker.pickImage(
                              source: ImageSource.gallery,
                              imageQuality: 50,
                              //preferredCameraDevice: CameraDevice.front
                         );
                          setState(() {
                            _image = File(image.path);
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration:
                              BoxDecoration(color: Colors.deepOrange[50]),
                          child: _image != null
                              ? Image.file(
                                  _image,
                                  width: 200.0,
                                  height: 200.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      color: Colors.deepOrange[50]),
                                  width: 200,
                                  height: 200,
                                  child: Icon(
                                    Icons.camera_enhance_rounded,
                                    color: Colors.grey[800],
                                  ),
                                ),
                        ),
                      ),
                    ),
                    buildTextfield('Enter foodname', foodname),
                    buildTextfield('Enter price', price),

                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          buildElevatedButton('Add'),
                          buildElevatedButton('Cancel'),
                        ],
                      ),
                    ),
                  ],

                ),

              ),
            ),
          ],
        ),
      ),
    );
  }

  ElevatedButton buildElevatedButton(String title) {
    return ElevatedButton(
      onPressed: () async {

        if (title == 'Add') {
           await DatabaseHelper.instance.add(
                  FoodModel(
                      foodname: foodname.text,
                      price: price.text,
                      image: _image.path),
                );
          setState(() {
            //firstname.clear();
            //selectedId = null;
          });

          Navigator.pop(context);
        }else Navigator.pop(context);
      }
      ,
      style: ElevatedButton.styleFrom(
          fixedSize: Size(120, 50),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          textStyle:
              const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      child: Text(
        title,
      ),
    );
  }

  Padding buildTextfield(String title, final ctrl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
      child: TextField(
        controller: ctrl,
        style: TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          hintText: title,
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }
}
