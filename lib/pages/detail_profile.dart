

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sqlite_project/database/database_helper.dart';
import 'package:sqlite_project/model/profile_model.dart';
import 'package:sqlite_project/pages/add_profile.dart';
import 'package:sqlite_project/pages/edit_profile.dart';
import 'package:sqlite_project/pages/show_profile.dart';

class ListFood extends StatefulWidget {
  const ListFood({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ListFood> createState() => _ListFoodState();
}

class _ListFoodState extends State<ListFood> {

  int? selectedId;

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: IconButton(
              icon: const Icon(Icons.add),
              tooltip: 'Add Profile',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddFood()),
                ).then((value){
                  //getAllData();
                  setState(() {

                  });
                });
              },

            ),
          ),
        ],
      ),

      body: Center(
        child: FutureBuilder<List<FoodModel>>(
            future: DatabaseHelper.instance.getFoods(),
            builder: (BuildContext context,
                AsyncSnapshot<List<FoodModel>> foods) {
              if (!foods.hasData) {
                return Center(child: Text('Loading...'));
              }
              return foods.data!.isEmpty
                  ? Center(child: Text('No foods in List.'))
                  : ListView(
                children: foods.data!.map((food) {
                  return Center(
                    child: Card(
                      color: selectedId == food.id
                          ? Colors.white70
                          : Colors.white,
                      child: ListTile(
                        title: Text(food.foodname),
                        subtitle: Text(food.price),
                        leading: CircleAvatar(backgroundImage: FileImage(File(food.image))),
                        //leading: Image(
                        //  image: FileImage(File(grocery.image)),
                        //  fit: BoxFit.cover,
                        //  height: 500,
                        //  width: 100,
                        //),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children:  <Widget>[
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.edit),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => EditFood(food)),
                                ).then((value){
                                  setState(() {
                                  });
                                });
                              },
                            ),
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.clear),
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: new Text("Do you want to delete this record?"),
                                      // content: new Text("Please Confirm"),
                                      actions: [
                                        new TextButton(
                                          onPressed: () {
                                            DatabaseHelper.instance.remove(food.id!);
                                            setState(() {
                                              Navigator.of(context).pop();
                                            });
                                          },
                                          child: new Text("Ok"),
                                        ),
                                        Visibility(
                                          visible: true,
                                          child: new TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: new Text("Cancel"),
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                );

                              },
                            ),
                          ],
                        ),
                        onTap: () {
                          var foodid = food.id;
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ShowFood(id: foodid)));

                          setState(() {
                            print(food.image);
                            if (selectedId == null) {
                              //firstname.text = grocery.firstname;
                              selectedId = food.id;
                            } else {
                              // textController.text = '';
                              selectedId = null;
                            }
                          });
                        },
                        onLongPress: () {
                          setState(() {
                            DatabaseHelper.instance.remove(food.id!);
                          });
                        },
                      ),
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}