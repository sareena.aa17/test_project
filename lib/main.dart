import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/src/material/colors.dart';
import 'package:flutter/widgets.dart';
import 'package:sqlite_project/pages/detail_profile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SQLite Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: const ListFood(title: 'Food Lists'),
      debugShowCheckedModeBanner: false,
    );
  }
}



